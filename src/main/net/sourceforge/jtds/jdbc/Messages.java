// jTDS JDBC Driver for Microsoft SQL Server and Sybase
// Copyright (C) 2004 The jTDS Project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
package net.sourceforge.jtds.jdbc;

import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;


/**
 * Support class for <code>Messages.properties</code>.
 *
 * @author David D. Kilzer
 * @author Mike Hutchinson
 * @version $Id: Messages.java,v 1.8 2005-04-20 16:49:22 alin_sinpalean Exp $
 */
public final class Messages {

    /**
     * Default name for resource bundle containing the messages.
     */
    private static final String DEFAULT_RESOURCE = "net.sourceforge.jtds.jdbc.Messages";

    /**
     * Cached resource bundle containing the messages.
     * <p>
     * <code>ResourceBundle</code> does caching internally but this caching
     * involves a lot of string operations to generate the keys used for
     * caching, leading to a lot of <code>StringBuffer</code> reallocation. In
     * one run through the complete jTDS test suite there were over 60000
     * allocations and reallocations (about one for each <code>get()</code>
     * call).
     */
    private static ResourceBundle defaultResource;
    private static Properties defaultProperties;


    /**
     * Default constructor.  Private to prevent instantiation.
     */
    private Messages() {
    }


    /**
     * Get runtime message using supplied key.
     *
     * @param key The key of the message in Messages.properties
     * @return The selected message as a <code>String</code>.
     */
    public static String get(String key) {
        return get(key, null);
    }


    /**
     * Get runtime message using supplied key and substitute parameter
     * into message.
     *
     * @param key The key of the message in Messages.properties
     * @param param1 The object to insert into message.
     * @return The selected message as a <code>String</code>.
     */
    public static String get(String key, Object param1) {
        Object args[] = {param1};
        return get(key, args);
    }


    /**
     * Get runtime message using supplied key and substitute parameters
     * into message.
     *
     * @param key The key of the message in Messages.properties
     * @param param1 The object to insert into message.
     * @param param2 The object to insert into message.
     * @return The selected message as a <code>String</code>.
     */
    static String get(String key, Object param1, Object param2) {
        Object args[] = {param1, param2};
        return get(key, args);
    }


    /**
     * Get runtime error using supplied key and substitute parameters
     * into message.
     *
     * @param key The key of the error message in Messages.properties
     * @param arguments The objects to insert into the message.
     * @return The selected error message as a <code>String</code>.
     */
    private static String get(String key, Object[] arguments) {
        try {
            //ResourceBundle bundle = loadResourceBundle();
        	//String formatString = bundle.getString(key);
            Properties props=getProperties();
            String formatString = props.getProperty(key);
            if (formatString==null) {
            	 throw new RuntimeException("No message resource found for message property " + key);
            }
            // No need for any formatting if no parameters are specified
            if (arguments == null || arguments.length == 0) {
                return formatString;
            } else {
                MessageFormat formatter = new MessageFormat(formatString);
                return formatter.format(arguments);
            }
        } catch (java.util.MissingResourceException mre) {
            throw new RuntimeException("No message resource found for message property " + key);
        }
    }


    private static Properties getProperties() {
		if (defaultProperties==null){
			Properties props=new Properties();
			props.setProperty("prop.appname","APPNAME");
			props.setProperty("prop.autocommit","AUTOCOMMIT");
			props.setProperty("prop.batchsize","BATCHSIZE");
			props.setProperty("prop.bindaddress","BINDADDRESS");
			props.setProperty("prop.buffermaxmemory","BUFFERMAXMEMORY");
			props.setProperty("prop.bufferminpackets","BUFFERMINPACKETS");
			props.setProperty("prop.cachemetadata","CACHEMETADATA");
			props.setProperty("prop.charset","CHARSET");
			props.setProperty("prop.databasename","DATABASENAME");
			props.setProperty("prop.domain","DOMAIN");
			props.setProperty("prop.instance","INSTANCE");
			props.setProperty("prop.language","LANGUAGE");
			props.setProperty("prop.lastupdatecount","LASTUPDATECOUNT");
			props.setProperty("prop.lobbuffer","LOBBUFFER");
			props.setProperty("prop.logfile","LOGFILE");
			props.setProperty("prop.logintimeout","LOGINTIMEOUT");
			props.setProperty("prop.macaddress","MACADDRESS");
			props.setProperty("prop.maxstatements","MAXSTATEMENTS");
			props.setProperty("prop.namedpipe","NAMEDPIPE");
			props.setProperty("prop.packetsize","PACKETSIZE");
			props.setProperty("prop.password","PASSWORD");
			props.setProperty("prop.portnumber","PORTNUMBER");
			props.setProperty("prop.preparesql","PREPARESQL");
			props.setProperty("prop.progname","PROGNAME");
			props.setProperty("prop.servername","SERVERNAME");
			props.setProperty("prop.servertype","SERVERTYPE");
			props.setProperty("prop.sotimeout","SOCKETTIMEOUT");
			props.setProperty("prop.sokeepalive","SOCKETKEEPALIVE");
			props.setProperty("prop.processid","PROCESSID");
			props.setProperty("prop.ssl","SSL");
			props.setProperty("prop.tcpnodelay","TCPNODELAY");
			props.setProperty("prop.tds","TDS");
			props.setProperty("prop.bufferdir","BUFFERDIR");
			props.setProperty("prop.usecursors","USECURSORS");
			props.setProperty("prop.usejcifs","USEJCIFS");
			props.setProperty("prop.uselobs","USELOBS");
			props.setProperty("prop.user","USER");
			props.setProperty("prop.useunicode","SENDSTRINGPARAMETERSASUNICODE");
			props.setProperty("prop.wsid","WSID");
			props.setProperty("prop.xaemulation","XAEMULATION");
			props.setProperty("prop.usentlmv2","USENTLMV2");
			props.setProperty("prop.usekerberos","USEKERBEROS");

			props.setProperty("prop.desc.appname","The application name advertised by the driver.");
			props.setProperty("prop.desc.autocommit","Set the autocommit mode of newly created connections.");
			props.setProperty("prop.desc.batchsize","The number of statements to submit at once. Batches are broken up in pieces this large.");
			props.setProperty("prop.desc.bindaddress","The IP address of the local interface that jTDS should bind to when connecting to a database via TCP/IP.");
			props.setProperty("prop.desc.buffermaxmemory","The global buffer memory limit for all connections (in kilobytes).");
			props.setProperty("prop.desc.bufferminpackets","The minimum number of packets per statement to buffer to memory.");
			props.setProperty("prop.desc.cachemetadata","Set to true to enable the caching of column meta data for prepared statements.");
			props.setProperty("prop.desc.charset","Server character set for non-Unicode character values.");
			props.setProperty("prop.desc.databasename","The database name.");
			props.setProperty("prop.desc.domain","The domain used for authentication.");
			props.setProperty("prop.desc.instance","The database server instance.");
			props.setProperty("prop.desc.language","The language for server messages.");
			props.setProperty("prop.desc.lastupdatecount","Return only the last update count on executeUpdate.");
			props.setProperty("prop.desc.lobbuffer","The amount of LOB data to buffer in memory before caching to disk.");
			props.setProperty("prop.desc.logfile","Set the name of a file for the capture of logging information.");
			props.setProperty("prop.desc.logintimeout","The time to wait for a successful login before timing out.");
			props.setProperty("prop.desc.macaddress","Hex-encoded client MAC address.");
			props.setProperty("prop.desc.maxstatements","The maximum number of statements to keep open. This is only a target for the cache.");
			props.setProperty("prop.desc.namedpipe","Use named pipes to connect instead of TCP/IP sockets.");
			props.setProperty("prop.desc.packetsize","The network packet size (a multiple of 512).");
			props.setProperty("prop.desc.password","The database password.");
			props.setProperty("prop.desc.portnumber","The database server port number.");
			props.setProperty("prop.desc.preparesql","Use stored procedures for prepared statements.");
			props.setProperty("prop.desc.progname","The program name advertised by the driver.");
			props.setProperty("prop.desc.servername","The database server hostname.");
			props.setProperty("prop.desc.servertype","The type of database (1 is SQL Server, 2 is Sybase).");
			props.setProperty("prop.desc.sotimeout","The TCP/IP socket timeout value in seconds or 0 for no timeout.");
			props.setProperty("prop.desc.sokeepalive","Use TCP/IP socket keep alive feature.");
			props.setProperty("prop.desc.processid","The process ID reported by the driver.");
			props.setProperty("prop.desc.ssl","Set the SSL level.");
			props.setProperty("prop.desc.tcpnodelay","Enable/disable TCP_NODELAY");
			props.setProperty("prop.desc.tds","The database server protocol.");
			props.setProperty("prop.desc.bufferdir","The directory to buffer data to.");
			props.setProperty("prop.desc.usecursors","Use SQL Server fast forward only result sets for forward only result sets.");
			props.setProperty("prop.desc.usejcifs","Force use of jCIFS library on Windows for connecting via named pipes.");
			props.setProperty("prop.desc.uselobs","Map large types (IMAGE and TEXT/NTEXT) to LOBs vs. String/byte[].");
			props.setProperty("prop.desc.user","The database user.");
			props.setProperty("prop.desc.useunicode","If strings should be sent as unicode values.");
			props.setProperty("prop.desc.wsid","Workstation ID or client host name override. Will be stored in master.dbo.sysprocesses, column hostname.");
			props.setProperty("prop.desc.xaemulation","Set to false to use the Microsoft Distributed Transaction Coordinator.");
			props.setProperty("prop.desc.usentlmv2","Set to true to send LMv2/NTLMv2 responses when using Windows authentication");
			props.setProperty("prop.desc.usekerberos","Set to true to use Kerberos as the authentication mechanism.|N|true,false");

			props.setProperty("error.baddatatype","The TDS protocol does not support JDBC datatype {0}.");
			props.setProperty("error.baddbname","The database name ''{0}'' is invalid.");
			props.setProperty("error.badoption","The supplied parameter value ''{0}'' is invalid.");
			props.setProperty("error.bintoolong","Sybase does not support binary parameters > 255 bytes.");
			props.setProperty("error.blob.badpattern","The search pattern cannot be null.");
			props.setProperty("error.blob.badstart","The start position must be >\",\" 1.");
			props.setProperty("error.blob.bytesnull","The bytes value must not be null.");
			props.setProperty("error.blobclob.badlen","The length must be >\",\" 0.");
			props.setProperty("error.blobclob.badoffset","The offset must be >\",\" 0 and less than the array length.");
			props.setProperty("error.blobclob.badpos","The start position must be >\",\" 1.");
			props.setProperty("error.blobclob.badposlen","The start position is beyond the end of the data.");
			props.setProperty("error.blobclob.lentoolong","The value of length exceeds that of the available data.");
			props.setProperty("error.blobclob.readlen","The amount of data read from the stream is not \",\" length.");
			props.setProperty("error.callable.noparam","Parameter ''{0}'' not found in the parameter list.");
			props.setProperty("error.callable.outparamnotset","Output parameters have not yet been processed. Call getMoreResults().");
			props.setProperty("error.callable.notoutput","Parameter ''{0}'' not registered as output parameter. Call registerOutParameter().");
			props.setProperty("error.charset.invalid","Charset {0}/{1} is not supported by the JVM.");
			props.setProperty("error.charset.nomapping","Could not find a Java charset equivalent to DB charset {0}.");
			props.setProperty("error.charset.nocollation","Could not find a Java charset equivalent to collation {0}.");
			props.setProperty("error.chartoolong","Sybase does not support char parameters > 255 bytes.");
			props.setProperty("error.clob.searchnull","The search string must not be null.");
			props.setProperty("error.clob.strnull","The string value must not be null.");
			props.setProperty("error.connection.badhost","Unknown server host name ''{0}''.");
			props.setProperty("error.connection.badprop","The {0} connection property is invalid.");
			props.setProperty("error.connection.badsavep","Savepoint is not valid for this transaction.");
			props.setProperty("error.connection.badxaop","Calls to the {0} method are not allowed in an XA transaction.");
			props.setProperty("error.connection.autocommit","{0}() should not be called while in auto-commit mode.");
			props.setProperty("error.connection.dbmismatch","Client {0} / Server {1} old database mismatch.");
			props.setProperty("error.connection.ioerror","Network error IOException: {0}");
			props.setProperty("error.connection.nohost","The serverName property has not been set.");
			props.setProperty("error.connection.savenorollback","Savepoints cannot be rolled back in auto-commit mode.");
			props.setProperty("error.connection.savenoset","Savepoints cannot be set in auto-commit mode.");
			props.setProperty("error.connection.savenullname","Savepoint name cannot be null.");
			props.setProperty("error.connection.servertype","The value of the serverType property is invalid: {0}");
			props.setProperty("error.connection.timeout","Login timed out.");
			props.setProperty("error.conproxy.noconn","Connection has been returned to pool and this reference is no longer valid.");
			props.setProperty("error.convert.numericoverflow","Numeric overflow in conversion of value {0} to {1}.");
			props.setProperty("error.convert.badnumber","Value {0} cannot be converted to {1}.");
			props.setProperty("error.convert.badtypeconst","Unable to convert value {0} to {1}.");
			props.setProperty("error.convert.badtypes","Unable to convert between {0} and {1}.");
			props.setProperty("error.cursoropen.fail","Cursor failed to open.");
			props.setProperty("error.datetime.range","Only dates between January 1, 1753 and December 31, 9999 are accepted.");
			props.setProperty("error.datetime.range.era","Only dates in the AD era are accepted.");
			props.setProperty("error.dbmeta.nouser","Unable to determine the user name.");
			props.setProperty("error.driver.badurl","The syntax of the connection URL ''{0}'' is invalid.");
			props.setProperty("error.generic.unspecified","The server returned an unspecified error.");
			props.setProperty("error.generic.badoption","Invalid option value ''{0}'' for parameter {1}.");
			props.setProperty("error.generic.badparam","Invalid value ''{0}'' for parameter {1}.");
			props.setProperty("error.generic.badscale","The decimal scale must be >\",\" 0 and <\",\" 28.");
			props.setProperty("error.generic.badtype","Invalid java.sql.Types constant value {0} passed to set or update method.");
			props.setProperty("error.generic.cancelled","Cancel has been invoked on this {0}.");
			props.setProperty("error.generic.closed","Invalid state, the {0} object is closed.");
			props.setProperty("error.generic.encoding","Unexpected encoding exception: {0}");
			props.setProperty("error.generic.ioerror","I/O Error: {0}");
			props.setProperty("error.generic.ioread","I/O Error reading {0}: {1}");
			props.setProperty("error.generic.iowrite","I/O Error writing {0}: {1}");
			props.setProperty("error.generic.needcolindex","The column indexes parameter should be int[1].");
			props.setProperty("error.generic.needcolname","The column names parameter should be String[1].");
			props.setProperty("error.generic.nosql","The SQL statement must not be null or empty.");
			props.setProperty("error.generic.notimp","The {0} method is not implemented.");
			props.setProperty("error.generic.notsup","Use of the {0} method is not supported on this type of statement.");
			props.setProperty("error.generic.nullparam","Null not permitted as parameter of method {0}");
			props.setProperty("error.generic.optltzero","The {0} method requires a parameter value >\",\" 0.");
			props.setProperty("error.generic.optvalue","The {0} option is not currently supported by the {1} method.");
			props.setProperty("error.generic.tdserror","TDS Protocol error: {0}");
			props.setProperty("error.generic.timeout","The query has timed out.");
			props.setProperty("error.generic.truncmbcs","MBCS Parameter truncated.");
			props.setProperty("error.jdbcx.conclosed","The pooled connection is closed.");
			props.setProperty("error.msinfo.badinfo","Unable to get information from SQL Server: {0}.");
			props.setProperty("error.msinfo.badinst","Server {0} has no instance named {1}.");
			props.setProperty("error.msinfo.badport","Could not parse instance port number ''{0}''.");
			props.setProperty("error.nooption","The {0} option is not supported.");
			props.setProperty("error.normalize.lobtoobig","The LOB length is greater than 2,147,483,647.");
			props.setProperty("error.normalize.numtoobig","BigDecimal value has more than {0} digits of precision.");
			props.setProperty("error.notimplemented","Method {0} is not implemented.");
			props.setProperty("error.parsesql.mustbe","Invalid JDBC escape syntax at line position {0} ''{1}'' character expected.");
			props.setProperty("error.parsesql.badesc","Unrecognized SQL escape ''{0}'' at line position {1}.");
			props.setProperty("error.parsesql.missing","Invalid SQL statement or JDBC escape, terminating ''{0}'' not found.");
			props.setProperty("error.parsesql.syntax","Invalid JDBC {0} escape at line position {1}.");
			props.setProperty("error.parsesql.toomanyparams","Prepared or callable statement has more than {0} parameter markers.");
			props.setProperty("error.parsesql.unexpectedparam","Unexpected parameter marker at position {0}.");
			props.setProperty("error.parsesql.noprocedurecall","The SQL statement has to contain a procedure call.");
			props.setProperty("error.prepare.nooutparam","Output parameter not allowed as argument list prevents use of RPC.");
			props.setProperty("error.prepare.nosql","The prepared statement parameter must contain a valid SQL statement.");
			props.setProperty("error.prepare.paramindex","Invalid parameter index {0}.");
			props.setProperty("error.prepare.paramnotset","Parameter #{0} has not been set.");
			props.setProperty("error.prepare.prepfailed","Preparing the statement failed: {0}");
			props.setProperty("error.resultset.badurl","The URL ''{0}'' is invalid.");
			props.setProperty("error.resultset.colindex","Invalid column index {0}.");
			props.setProperty("error.resultset.colname","Invalid column name {0}.");
			props.setProperty("error.resultset.cursorfail","Cursor operation failed.");
			props.setProperty("error.resultset.delete","Non-null row provided to delete operation.");
			props.setProperty("error.resultset.fwdonly","ResultSet may only be accessed in a forward direction.");
			props.setProperty("error.resultset.insert","Column {0} / {1} is read-only.");
			props.setProperty("error.resultset.insrow","The cursor is on the insert row.");
			props.setProperty("error.resultset.longblob","Blob lengths greater than 2,147,483,647 are not supported.");
			props.setProperty("error.resultset.longclob","Clob lengths greater than 2,147,483,647 are not supported.");
			props.setProperty("error.resultset.noposupdate","Positioned update not supported.");
			props.setProperty("error.resultset.norow","No current row in the ResultSet.");
			props.setProperty("error.resultset.notinsrow","The cursor is not on the insert row.");
			props.setProperty("error.resultset.openfail","Unable to open the specified type of cursor.");
			props.setProperty("error.resultset.readonly","ResultSet is read only.");
			props.setProperty("error.resultset.streamerror","setBinaryStream: IO-Exception occurred reading Stream: {0}");
			props.setProperty("error.resultset.streamlen","setBinaryStream parameterized length: {0} got length: {1}.");
			props.setProperty("error.resultset.streamlen2","setBinaryStream parameterized length: {0} got more than that.");
			props.setProperty("error.resultset.update","Null row provided to insert/update operation.");
			props.setProperty("error.resultset.updatefail","Update row failed. unable to locate row to update in table.");
			props.setProperty("error.resultset.deletefail","Delete row failed. Unable to locate row to delete in table.");
			props.setProperty("error.resultset.insertfail","Insert row failed.");
			props.setProperty("error.savepoint.named","This is a named savepoint.");
			props.setProperty("error.savepoint.unnamed","This is an unnamed savepoint.");
			props.setProperty("error.ssl.encryptionoff","Encryption is not enabled at the server.");
			props.setProperty("error.statement.badbatch","Unable to execute batch of type {0}.");
			props.setProperty("error.statement.badsql","The SQL statement must not contain a procedure call or parameter markers.");
			props.setProperty("error.statement.batchnocount","A statement attempted to return a result set in executeBatch().");
			props.setProperty("error.statement.gtmaxrows","The requested fetch size is greater than the maximum number of rows.");
			props.setProperty("error.statement.needcolindex","One valid column index must be supplied.");
			props.setProperty("error.statement.needcolname","One valid column name must be supplied.");
			props.setProperty("error.statement.nocount","The executeUpdate method must not return a result set.");
			props.setProperty("error.statement.nodata","There are no results to process.");
			props.setProperty("error.statement.noresult","The executeQuery method must return a result set.");
			props.setProperty("error.textoutparam","Output parameters must not have a type of text, ntext or image.");
			props.setProperty("error.tdscore.badlen","Unable to determine the length of text or image field {0}.");
			props.setProperty("error.tdscore.badtext","No table and / or column name is available for this text or image column.");
			props.setProperty("error.tdscore.notextptr","There is no text pointer available for text or image column {0}.");
			props.setProperty("error.update.results","executeUpdate() must not return a result set.");
			props.setProperty("error.xaexception.xaerunknown","The XA resource manager has reported an unknown error.");
			props.setProperty("error.xaexception.xarbrollback","XA_RBROLLBACK: The rollback was caused by an unspecified reason.");
			props.setProperty("error.xaexception.xarbcommfail","XA_RBCOMMFAIL: The rollback was caused by a communication failure.");
			props.setProperty("error.xaexception.xarbdeadlock","XA_RBDEADLOCK: A deadlock was detected.");
			props.setProperty("error.xaexception.xarbintegrity","XA_RBINTEGRITY: A condition that violates the integrity of the resources was detected.");
			props.setProperty("error.xaexception.xarbother","XA_RBOTHER: The resource manager rolled back the transaction branch for an unknown reason.");
			props.setProperty("error.xaexception.xarbproto","XA_RBPROTO: A protocol error occurred in the resource manager.");
			props.setProperty("error.xaexception.xarbtimeout","XA_RBTIMEOUT: A transaction branch took too long.");
			props.setProperty("error.xaexception.xarbtransient","XA_RBTRANSIENT: TM may retry the transaction branch.");
			props.setProperty("error.xaexception.xanomigrate","XA_NOMIGRATE: Transaction must be resumed where suspension occurred.");
			props.setProperty("error.xaexception.xaheurhaz","XA_HEURHAZ: The transaction branch may have been heuristically completed.");
			props.setProperty("error.xaexception.xaheurcom","XA_HEURCOM: The transaction branch has been heuristically committed.");
			props.setProperty("error.xaexception.xaheurrb","XA_HEURRB: The transaction branch has been heuristically rolled back.");
			props.setProperty("error.xaexception.xaheurmix","XA_HEURMIX: The transaction branch has been heuristically committed and rolled back.");
			props.setProperty("error.xaexception.xaretry","XA_RETRY: Function call returned with no effect and may be reissued.");
			props.setProperty("error.xaexception.xardonly","XA_RDONLY: The transaction branch was read-only and has been committed.");
			props.setProperty("error.xaexception.xaerasync","XAER_ASYNC: An asynchronous operation is already outstanding.");
			props.setProperty("error.xaexception.xaerrmerr","XAER_RMERR: A resource manager error occurred in the transaction branch.");
			props.setProperty("error.xaexception.xaernota","XAER_NOTA: The XID does not identify a valid transaction.");
			props.setProperty("error.xaexception.xaerinval","XAER_INVAL: Invalid arguments were given.");
			props.setProperty("error.xaexception.xaerproto","XAER_PROTO: Function invoked in an improper context.");
			props.setProperty("error.xaexception.xaerrmfail","XAER_RMFAIL: The resource manager is unavailable.");
			props.setProperty("error.xaexception.xaerdupid","XAER_DUPID: The XID identifies an existing transaction.");
			props.setProperty("error.xaexception.xaeroutside","XAER_OUTSIDE: The resource manager is doing work outside this transaction.");
			props.setProperty("error.xasupport.badopen","The xa_open call has failed, check that the MSDTC service is available.");
			props.setProperty("error.xasupport.nodist","True distributed transactions are only supported with SQL Server 2000.");
			props.setProperty("error.xasupport.activetran","The connection is still enlisted in a transaction when {0} called.");
			props.setProperty("warning.concurrtype","Don''t know how to handle concurrency type {0}.");
			props.setProperty("warning.cursordowngraded","ResultSet type/concurrency downgraded: {0}");
			props.setProperty("warning.cursortype","Don''t know how to handle cursor type {0}.");

			defaultProperties=props;		
		}
		return defaultProperties;
	}


	/**
     * Retrieve the list of driver property names and driver property
     * descriptions from <code>Messages.properties</code> and populate
     * them into {@link Map} objects.
     * <p/>
     * The keys used to populate both <code>propertyMap</code> and
     * <code>descriptionMap</code> are guaranteed to match up as long
     * as the properties defined in <code>Messages.properties</code>
     * are well-formed.
     *
     * @param propertyMap The map of property names to be populated.
     * @param descriptionMap The map of property descriptions to be populated.
     */
    static void loadDriverProperties(Map propertyMap, Map descriptionMap) {
        final ResourceBundle bundle = loadResourceBundle();
        final Enumeration keys = bundle.getKeys();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            final String descriptionPrefix = "prop.desc.";
            final String propertyPrefix = "prop.";
            if (key.startsWith(descriptionPrefix)) {
                descriptionMap.put(key.substring(descriptionPrefix.length()), bundle.getString(key));
            }
            else if (key.startsWith(propertyPrefix)) {
                propertyMap.put(key.substring(propertyPrefix.length()), bundle.getString(key));
            }
        }
    }


    /**
     * Load the {@link #DEFAULT_RESOURCE} resource bundle.
     *
     * @return The resource bundle.
     */
    private static ResourceBundle loadResourceBundle() {
        if (defaultResource == null) {
            defaultResource = ResourceBundle.getBundle(DEFAULT_RESOURCE);
        }
        return defaultResource;
    }

}
