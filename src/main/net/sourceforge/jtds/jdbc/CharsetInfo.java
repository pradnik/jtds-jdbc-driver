// jTDS JDBC Driver for Microsoft SQL Server and Sybase
// Copyright (C) 2004 The jTDS Project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
package net.sourceforge.jtds.jdbc;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Enumeration;
import java.io.InputStream;
import java.io.IOException;

import net.sourceforge.jtds.util.Logger;

/**
 * Loads and stores information about character sets. Static fields and methods
 * are concerned with loading, storing and retrieval of all character set
 * information, while non-static fields and methods describe a particular
 * character set (Java charset name and whether it's a multi-byte charset).
 * <p>
 * <b>Note:</b> Only one <code>CharsetInfo</code> instance exists per charset.
 * This allows simple equality comparisons between instances retrieved with any
 * of the <code>get</code> methods.
 *
 * @author Alin Sinpalean
 * @version $Id: CharsetInfo.java,v 1.5 2007-07-08 17:28:23 bheineman Exp $
 */
public final class CharsetInfo {
    //
    // Static fields and methods
    //

    /** Name of the <code>Charsets.properties</code> resource. */
    private static final String CHARSETS_RESOURCE_NAME
            = "net/sourceforge/jtds/jdbc/Charsets.properties";

    /** Server charset to Java charset map. */
    private static final HashMap charsets = new HashMap();

    /** Locale id to Java charset map. */
    private static final HashMap lcidToCharsetMap = new HashMap();

    /** Sort order to Java charset map. */
    private static final CharsetInfo[] sortToCharsetMap = new CharsetInfo[256];

    static {
        // Load character set mappings
        InputStream stream = null;
        try {
           
                Properties tmp = new Properties();
                
                tmp.setProperty("ISO_1","1|ISO-8859-1");
                tmp.setProperty("ISO15","1|ISO-8859-15");
                tmp.setProperty("ISO88592","1|ISO-8859-2");
                tmp.setProperty("ISO88595","1|ISO-8859-5");
                tmp.setProperty("ISO88596","1|ISO-8859-6");
                tmp.setProperty("ISO88597","1|ISO-8859-7");
                tmp.setProperty("ISO88598","1|ISO-8859-8");
                tmp.setProperty("ISO88599","1|ISO-8859-9");
                tmp.setProperty("CP437","1|Cp437");
                tmp.setProperty("CP850","1|Cp850");
                tmp.setProperty("CP932","2|MS932");
                tmp.setProperty("CP936","2|MS936");
                tmp.setProperty("CP949","2|Cp949");
                tmp.setProperty("CP950","2|Cp950");
                tmp.setProperty("CP1250","1|Cp1250");
                tmp.setProperty("CP1251","1|Cp1251");
                tmp.setProperty("CP1252","1|Cp1252");
                tmp.setProperty("CP1253","1|Cp1253");
                tmp.setProperty("CP1254","1|Cp1254");
                tmp.setProperty("CP1255","1|Cp1255");
                tmp.setProperty("CP1256","1|Cp1256");
                tmp.setProperty("CP1257","1|Cp1257");
                tmp.setProperty("CP1258","1|Cp1258");
                tmp.setProperty("ASCII_7","1|US-ASCII");
                tmp.setProperty("ASCII_8","1|US-ASCII");
                tmp.setProperty("ROMAN8","1|ISO-8859-1");
                tmp.setProperty("MAC","1|MacRoman");
                tmp.setProperty("GREEK8","1|ISO-8859-7");
                tmp.setProperty("MACGRK2","1|ISO-8859-7");
                tmp.setProperty("MAC_CYR","1|ISO-8859-5");
                tmp.setProperty("SJIS","2|Shift_JIS");
                tmp.setProperty("BIG5","2|Big5");
                tmp.setProperty("BIG5HK","2|Big5-HKSCS");
                tmp.setProperty("GB2312","2|MS936");
                tmp.setProperty("GBK","2|MS936");
                tmp.setProperty("KOI8","1|KOI8-R");
                tmp.setProperty("TIS620","1|TIS-620");
                tmp.setProperty("DECKANJI","3|EUC_JP");
                tmp.setProperty("EUCCNS","2|x-EUC-CN");
                tmp.setProperty("EUCJIS","3|EUC-JP");
                tmp.setProperty("EUCGB","2|x-EUC-CN");
                tmp.setProperty("EUCKSC","2|EUC-KR");
                tmp.setProperty("UTF8","4|UTF-8");
                tmp.setProperty("UNICODE","4|UTF-16");

                tmp.setProperty("ISO-8859-1","1|ISO-8859-1");
                tmp.setProperty("ISO-8859-2","1|ISO-8859-2");
                tmp.setProperty("ISO-8859-5","1|ISO-8859-5");
                tmp.setProperty("ISO-8859-6","1|ISO-8859-6");
                tmp.setProperty("ISO-8859-7","1|ISO-8859-7");
                tmp.setProperty("ISO-8859-8","1|ISO-8859-8");
                tmp.setProperty("ISO-8859-9","1|ISO-8859-9");
                tmp.setProperty("ISO-8859-15","1|ISO-8859-15");
                tmp.setProperty("US-ASCII","1|US-ASCII");
                tmp.setProperty("ASCII","1|US-ASCII");
                tmp.setProperty("SHIFT_JIS","2|Shift_JIS");
                tmp.setProperty("BIG5-HKSCS","2|Big5-HKSCS");
                tmp.setProperty("BIG5_HKSCS","2|Big5-HKSCS");
                tmp.setProperty("KOI8-R","1|KOI8-R");
                tmp.setProperty("KOI8_R","1|KOI8-R");
                tmp.setProperty("TIS-620","1|TIS-620");
                tmp.setProperty("TIS_620","1|TIS-620");
                tmp.setProperty("EUC-JP","3|EUC_JP");
                tmp.setProperty("EUC_JP","3|EUC_JP");
                tmp.setProperty("X-EUC-CN","2|x-EUC-CN");
                tmp.setProperty("EUC_CN","2|x-EUC-CN");
                tmp.setProperty("EUC-KR","2|EUC-KR");
                tmp.setProperty("EUC_KR","2|EUC-KR");
                tmp.setProperty("UTF-8","4|UTF-8");
                tmp.setProperty("UTF-16","4|UTF-16");
                tmp.setProperty("MACROMAN","1|MacRoman");

                tmp.setProperty("LCID_1054","2|MS874");
                tmp.setProperty("LCID_1041","2|MS932");
                tmp.setProperty("LCID_66577","2|MS932");
                tmp.setProperty("LCID_2052","2|MS936");
                tmp.setProperty("LCID_4100","2|MS936");
                tmp.setProperty("LCID_133124","2|MS936");
                tmp.setProperty("LCID_1042","2|MS949");
                tmp.setProperty("LCID_66578","2|MS949");
                tmp.setProperty("LCID_1028","2|MS950");
                tmp.setProperty("LCID_197636","2|MS950");
                tmp.setProperty("LCID_1029","1|Cp1250");
                tmp.setProperty("LCID_1038","1|Cp1250");
                tmp.setProperty("LCID_1045","1|Cp1250");
                tmp.setProperty("LCID_1048","1|Cp1250");
                tmp.setProperty("LCID_1050","1|Cp1250");
                tmp.setProperty("LCID_1051","1|Cp1250");
                tmp.setProperty("LCID_1052","1|Cp1250");
                tmp.setProperty("LCID_1060","1|Cp1250");
                tmp.setProperty("LCID_66574","1|Cp1250");
                tmp.setProperty("LCID_1026","1|Cp1251");
                tmp.setProperty("LCID_1049","1|Cp1251");
                tmp.setProperty("LCID_1058","1|Cp1251");
                tmp.setProperty("LCID_1059","1|Cp1251");
                tmp.setProperty("LCID_1071","1|Cp1251");
                tmp.setProperty("LCID_2074","1|Cp1251");
                tmp.setProperty("LCID_3098","1|Cp1251");
                tmp.setProperty("LCID_1027","1|Cp1252");
                tmp.setProperty("LCID_1030","1|Cp1252");
                tmp.setProperty("LCID_1031","1|Cp1252");
                tmp.setProperty("LCID_1033","1|Cp1252");
                tmp.setProperty("LCID_1034","1|Cp1252");
                tmp.setProperty("LCID_1035","1|Cp1252");
                tmp.setProperty("LCID_1036","1|Cp1252");
                tmp.setProperty("LCID_1039","1|Cp1252");
                tmp.setProperty("LCID_1040","1|Cp1252");
                tmp.setProperty("LCID_1043","1|Cp1252");
                tmp.setProperty("LCID_1044","1|Cp1252");
                tmp.setProperty("LCID_1046","1|Cp1252");
                tmp.setProperty("LCID_1053","1|Cp1252");
                tmp.setProperty("LCID_1057","1|Cp1252");
                tmp.setProperty("LCID_1069","1|Cp1252");
                tmp.setProperty("LCID_1080","1|Cp1252");
                tmp.setProperty("LCID_2055","1|Cp1252");
                tmp.setProperty("LCID_2057","1|Cp1252");
                tmp.setProperty("LCID_2058","1|Cp1252");
                tmp.setProperty("LCID_2060","1|Cp1252");
                tmp.setProperty("LCID_2064","1|Cp1252");
                tmp.setProperty("LCID_2067","1|Cp1252");
                tmp.setProperty("LCID_2068","1|Cp1252");
                tmp.setProperty("LCID_2070","1|Cp1252");
                tmp.setProperty("LCID_3097","1|Cp1252");
                tmp.setProperty("LCID_3081","1|Cp1252");
                tmp.setProperty("LCID_3082","1|Cp1252");
                tmp.setProperty("LCID_3084","1|Cp1252");
                tmp.setProperty("LCID_4103","1|Cp1252");
                tmp.setProperty("LCID_4105","1|Cp1252");
                tmp.setProperty("LCID_4106","1|Cp1252");
                tmp.setProperty("LCID_4108","1|Cp1252");
                tmp.setProperty("LCID_5127","1|Cp1252");
                tmp.setProperty("LCID_5129","1|Cp1252");
                tmp.setProperty("LCID_5130","1|Cp1252");
                tmp.setProperty("LCID_5132","1|Cp1252");
                tmp.setProperty("LCID_6153","1|Cp1252");
                tmp.setProperty("LCID_6154","1|Cp1252");
                tmp.setProperty("LCID_7178","1|Cp1252");
                tmp.setProperty("LCID_7717","1|Cp1252");
                tmp.setProperty("LCID_8201","1|Cp1252");
                tmp.setProperty("LCID_8202","1|Cp1252");
                tmp.setProperty("LCID_9225","1|Cp1252");
                tmp.setProperty("LCID_9226","1|Cp1252");
                tmp.setProperty("LCID_10250","1|Cp1252");
                tmp.setProperty("LCID_11274","1|Cp1252");
                tmp.setProperty("LCID_12298","1|Cp1252");
                tmp.setProperty("LCID_13322","1|Cp1252");
                tmp.setProperty("LCID_14346","1|Cp1252");
                tmp.setProperty("LCID_15370","1|Cp1252");
                tmp.setProperty("LCID_16394","1|Cp1252");
                tmp.setProperty("LCID_66567","1|Cp1252");
                tmp.setProperty("LCID_66615","1|Cp1252");
                tmp.setProperty("LCID_1032","1|Cp1253");
                tmp.setProperty("LCID_1055","1|Cp1254");
                tmp.setProperty("LCID_1037","1|Cp1255");
                tmp.setProperty("LCID_1025","1|Cp1256");
                tmp.setProperty("LCID_1056","1|Cp1256");
                tmp.setProperty("LCID_1065","1|Cp1256");
                tmp.setProperty("LCID_2049","1|Cp1256");
                tmp.setProperty("LCID_3073","1|Cp1256");
                tmp.setProperty("LCID_4097","1|Cp1256");
                tmp.setProperty("LCID_5121","1|Cp1256");
                tmp.setProperty("LCID_5145","1|Cp1256");
                tmp.setProperty("LCID_7169","1|Cp1256");
                tmp.setProperty("LCID_8193","1|Cp1256");
                tmp.setProperty("LCID_9217","1|Cp1256");
                tmp.setProperty("LCID_10241","1|Cp1256");
                tmp.setProperty("LCID_11265","1|Cp1256");
                tmp.setProperty("LCID_12289","1|Cp1256");
                tmp.setProperty("LCID_13313","1|Cp1256");
                tmp.setProperty("LCID_14337","1|Cp1256");
                tmp.setProperty("LCID_15361","1|Cp1256");
                tmp.setProperty("LCID_16385","1|Cp1256");
                tmp.setProperty("LCID_1061","1|Cp1257");
                tmp.setProperty("LCID_1062","1|Cp1257");
                tmp.setProperty("LCID_1063","1|Cp1257");
                tmp.setProperty("LCID_2087","1|Cp1257");
                tmp.setProperty("LCID_1066","1|Cp1258");

                tmp.setProperty("SORT_50","1|Cp1252");
                tmp.setProperty("SORT_51","1|Cp1252");
                tmp.setProperty("SORT_52","1|Cp1252");
                tmp.setProperty("SORT_53","1|Cp1252");
                tmp.setProperty("SORT_54","1|Cp1252");
                tmp.setProperty("SORT_71","1|Cp1252");
                tmp.setProperty("SORT_72","1|Cp1252");
                tmp.setProperty("SORT_183","1|Cp1252");
                tmp.setProperty("SORT_184","1|Cp1252");
                tmp.setProperty("SORT_185","1|Cp1252");
                tmp.setProperty("SORT_186","1|Cp1252");
                tmp.setProperty("SORT_210","1|Cp1252");
                tmp.setProperty("SORT_211","1|Cp1252");
                tmp.setProperty("SORT_212","1|Cp1252");
                tmp.setProperty("SORT_213","1|Cp1252");
                tmp.setProperty("SORT_214","1|Cp1252");
                tmp.setProperty("SORT_215","1|Cp1252");
                tmp.setProperty("SORT_216","1|Cp1252");
                tmp.setProperty("SORT_217","1|Cp1252");
                tmp.setProperty("SORT_40","1|Cp850");
                tmp.setProperty("SORT_41","1|Cp850");
                tmp.setProperty("SORT_42","1|Cp850");
                tmp.setProperty("SORT_43","1|Cp850");
                tmp.setProperty("SORT_44","1|Cp850");
                tmp.setProperty("SORT_49","1|Cp850");
                tmp.setProperty("SORT_55","1|Cp850");
                tmp.setProperty("SORT_56","1|Cp850");
                tmp.setProperty("SORT_57","1|Cp850");
                tmp.setProperty("SORT_58","1|Cp850");
                tmp.setProperty("SORT_59","1|Cp850");
                tmp.setProperty("SORT_60","1|Cp850");
                tmp.setProperty("SORT_61","1|Cp850");
                tmp.setProperty("SORT_30","1|Cp437");
                tmp.setProperty("SORT_31","1|Cp437");
                tmp.setProperty("SORT_32","1|Cp437");
                tmp.setProperty("SORT_33","1|Cp437");
                tmp.setProperty("SORT_34","1|Cp437");
                tmp.setProperty("SORT_80","1|Cp1250");
                tmp.setProperty("SORT_81","1|Cp1250");
                tmp.setProperty("SORT_82","1|Cp1250");
                tmp.setProperty("SORT_83","1|Cp1250");
                tmp.setProperty("SORT_84","1|Cp1250");
                tmp.setProperty("SORT_85","1|Cp1250");
                tmp.setProperty("SORT_86","1|Cp1250");
                tmp.setProperty("SORT_87","1|Cp1250");
                tmp.setProperty("SORT_88","1|Cp1250");
                tmp.setProperty("SORT_89","1|Cp1250");
                tmp.setProperty("SORT_90","1|Cp1250");
                tmp.setProperty("SORT_91","1|Cp1250");
                tmp.setProperty("SORT_92","1|Cp1250");
                tmp.setProperty("SORT_93","1|Cp1250");
                tmp.setProperty("SORT_94","1|Cp1250");
                tmp.setProperty("SORT_95","1|Cp1250");
                tmp.setProperty("SORT_96","1|Cp1250");
                tmp.setProperty("SORT_104","1|Cp1251");
                tmp.setProperty("SORT_105","1|Cp1251");
                tmp.setProperty("SORT_106","1|Cp1251");
                tmp.setProperty("SORT_107","1|Cp1251");
                tmp.setProperty("SORT_108","1|Cp1251");
                tmp.setProperty("SORT_112","1|Cp1253");
                tmp.setProperty("SORT_113","1|Cp1253");
                tmp.setProperty("SORT_114","1|Cp1253");
                tmp.setProperty("SORT_120","1|Cp1253");
                tmp.setProperty("SORT_121","1|Cp1253");
                tmp.setProperty("SORT_124","1|Cp1253");
                tmp.setProperty("SORT_128","1|Cp1254");
                tmp.setProperty("SORT_129","1|Cp1254");
                tmp.setProperty("SORT_130","1|Cp1254");
                tmp.setProperty("SORT_136","1|Cp1255");
                tmp.setProperty("SORT_137","1|Cp1255");
                tmp.setProperty("SORT_138","1|Cp1255");
                tmp.setProperty("SORT_144","1|Cp1256");
                tmp.setProperty("SORT_145","1|Cp1256");
                tmp.setProperty("SORT_146","1|Cp1256");
                tmp.setProperty("SORT_152","1|Cp1257");
                tmp.setProperty("SORT_153","1|Cp1257");
                tmp.setProperty("SORT_154","1|Cp1257");
                tmp.setProperty("SORT_155","1|Cp1257");
                tmp.setProperty("SORT_156","1|Cp1257");
                tmp.setProperty("SORT_157","1|Cp1257");
                tmp.setProperty("SORT_158","1|Cp1257");
                tmp.setProperty("SORT_159","1|Cp1257");
                tmp.setProperty("SORT_160","1|Cp1257");
                tmp.setProperty("SORT_194","2|MS949");
                tmp.setProperty("SORT_195","2|MS949");
                tmp.setProperty("SORT_201","2|MS949");
                tmp.setProperty("SORT_196","2|MS950");
                tmp.setProperty("SORT_197","2|MS950");
                tmp.setProperty("SORT_202","2|MS950");
                tmp.setProperty("SORT_198","2|MS936");
                tmp.setProperty("SORT_199","2|MS936");
                tmp.setProperty("SORT_203","2|MS936");
                tmp.setProperty("SORT_204","2|MS874");
                tmp.setProperty("SORT_205","2|MS874");
                tmp.setProperty("SORT_206","2|MS874");
                tmp.setProperty("SORT_192","2|MS932");
                tmp.setProperty("SORT_193","2|MS932");
                tmp.setProperty("SORT_200","2|MS932");

                
                HashMap instances = new HashMap();

                for (Enumeration e = tmp.propertyNames(); e.hasMoreElements();) {
                    String key = (String) e.nextElement();
                    CharsetInfo value = new CharsetInfo(tmp.getProperty(key));

                    // Ensure only one CharsetInfo instance exists per charset
                    CharsetInfo prevInstance = (CharsetInfo) instances.get(
                            value.getCharset());
                    if (prevInstance != null) {
                        if (prevInstance.isWideChars() != value.isWideChars()) {
                            throw new IllegalStateException(
                                    "Inconsistent Charsets.properties");
                        }
                        value = prevInstance;
                    }

                    if (key.startsWith("LCID_")) {
                        Integer lcid = new Integer(key.substring(5));
                        lcidToCharsetMap.put(lcid, value);
                    } else if (key.startsWith("SORT_")) {
                        sortToCharsetMap[Integer.parseInt(key.substring(5))] = value;
                    } else {
                        charsets.put(key, value);
                    }
                }
            
        } catch (Exception e) {
            // Can't load properties file for some reason
            Logger.logException(e);
        } finally {
            if (stream!=null) {
                try {
                    stream.close();
                } catch (Exception e) {
                 // errors can be ignored
                }
            }
        }
    }

    /**
     * Retrieves the <code>CharsetInfo</code> instance asociated with the
     * specified server charset.
     *
     * @param serverCharset the server-specific character set name
     * @return the associated <code>CharsetInfo</code>
     */
    public static CharsetInfo getCharset(String serverCharset) {
        return (CharsetInfo) charsets.get(serverCharset.toUpperCase());
    }

    /**
     * Retrieves the <code>CharsetInfo</code> instance asociated with the
     * specified LCID.
     *
     * @param lcid the server LCID
     * @return the associated <code>CharsetInfo</code>
     */
    public static CharsetInfo getCharsetForLCID(int lcid) {
        return (CharsetInfo) lcidToCharsetMap.get(new Integer(lcid));
    }

    /**
     * Retrieves the <code>CharsetInfo</code> instance asociated with the
     * specified sort order.
     *
     * @param sortOrder the server sort order
     * @return the associated <code>CharsetInfo</code>
     */
    public static CharsetInfo getCharsetForSortOrder(int sortOrder) {
        return sortToCharsetMap[sortOrder];
    }

    /**
     * Retrieves the <code>CharsetInfo</code> instance asociated with the
     * specified collation.
     *
     * @param collation the server LCID
     * @return the associated <code>CharsetInfo</code>
     */
    public static CharsetInfo getCharset(byte[] collation)
            throws SQLException {
        CharsetInfo charset;

        if (collation[4] != 0) {
            // The charset is determined by the sort order
            charset = getCharsetForSortOrder((int) collation[4] & 0xFF);
        } else {
            // The charset is determined by the LCID
            charset = getCharsetForLCID(
                    ((int) collation[2] & 0x0F) << 16
                    | ((int) collation[1] & 0xFF) << 8
                    | ((int) collation[0] & 0xFF));
        }

        if (charset == null) {
            throw new SQLException(
                    Messages.get("error.charset.nocollation", Support.toHex(collation)),
                    "2C000");
        }

        return charset;
    }

    //
    // Non-static fields and methods
    //

    /** The Java character set name. */
    private final String charset;
    /** Indicates whether current charset is wide (ie multi-byte). */
    private final boolean wideChars;

    /**
     * Constructs a <code>CharsetInfo</code> object from a character set
     * descriptor of the form: charset preceded by a numeric value indicating
     * whether it's a multibyte character set (&gt;1) or not (1) and a vertical
     * bar (|), eg &quot;1|Cp1252&quot; or &quot;2|MS936&quot;.
     *
     * @param descriptor the charset descriptor
     */
    public CharsetInfo(String descriptor) {
        wideChars = !"1".equals(descriptor.substring(0, 1));
        charset = descriptor.substring(2);
    }

    /**
     * Retrieves the charset name.
     */
    public String getCharset() {
        return charset;
    }

    /**
     * Retrieves whether the caracter set is wide (ie multi-byte).
     */
    public boolean isWideChars() {
        return wideChars;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CharsetInfo)) {
            return false;
        }

        final CharsetInfo charsetInfo = (CharsetInfo) o;
        if (!charset.equals(charsetInfo.charset)) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        return charset.hashCode();
    }

    public String toString() {
        return charset;
    }
}
